#define FILENAME "ringbuffer.c"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "error.h"
#include "types.h"
#include "ringbuffer.h"

/* Run-time assertions - good idea to check this function before doing 
   anything */
int ringbuffer_assert()
{
	if (!((sizeof(void*) == sizeof(char*)) &&
	      (sizeof(void*) == sizeof(u8*)))) {
		return 1;
	}
	if (!(sizeof(char) == 1)) { 
		return 1;
	}
	return SUCCESS;
}

/* Find the position of a character in a ringbuffer. This transforms unbounded
   read/write pointers to a bounded unsigned integer. */
size_t ringbuffer_findchar(struct ringbuffer* in, char c,
			   size_t i, size_t end)
{
	size_t start;
	if (!in) {
		return 0;
	}
	start = i % in->len;
	do {
		/* Terminate successfully on finding the character */
		if (in->data_char[i % in->len] == c) {
			return i % in->len;
		}
		i++;
		/* Terminate unsuccessfully on reaching the end pointer */
		if (i % in->len == end % in->len) {
			return 0;
		}
	} while (i % in->len != start);
	/* Terminate unsuccesfully on reaching the end of the buffer */
	return 0;
}

int ringbuffer_findstring(size_t* out, struct ringbuffer* r, char* in,
			  size_t inlen)
{
	size_t start;
	size_t f;
	size_t match_start;
	size_t i;
	size_t end;
	if (!r) {
		return ENULLPTR;
	}
	i = r->read;
	end = r->write;
	start = i;
	do {
		f = 0;
		match_start = i;
		while (r->data_char[i % r->len] == in[f]) {
			if (f == inlen - 1) {
				*out = match_start;
				return SUCCESS;
			}
			if (i == start || i == end) {
				return ENOTFOUND;
			}
			f++;
			i++;
		}
		i++;
	} while (i != start && i != end);
	return ENOTFOUND;
}

/* Return how much total free space is currently in a ringbuffer */
size_t ringbuffer_available(struct ringbuffer* in)
{
	if (!in) {
		return 0;
	}
	if (in->read < in->write) {
		return in->len - ((in->write - in->read) % in->len);

	}
	if (in->read == in->write) {
		/* Empty */
		return in->len;
	}
	/* read > write */
	return (in->read - in->write) % in->len;
}

/* Return how much data is in the ringbuffer to be read */
size_t ringbuffer_ready(struct ringbuffer* in)
{
	if (!in) {
		return 0;
	}
	if ((in->write - in->read) == in->len) {
		/* Full */
		return in->len;
	}
	return in->len - ringbuffer_available(in);
}

int ringbuffer_new(struct ringbuffer** out, size_t len)
{
	struct ringbuffer* p;
	if (!out) {
		return ENULLPTR;
	}
	p = malloc(sizeof(struct ringbuffer));
	if (!p) {
		return EALLOC;
	}
	memset(p, 0, sizeof(struct ringbuffer));
	p->data = malloc(len);
	if (!p->data) {
		free(p);
		return EALLOC;
	}
	memset(p->data, 0, len);
	p->len = len;
	*out = p;
	return SUCCESS;
}

void ringbuffer_free(struct ringbuffer* in)
{
	if (!in) {
		return;
	}
	if (in->data) {
		free(in->data);
	}
	free(in);
}

static void ringbuffer_push_byte(struct ringbuffer* buf, u8 byte)
{
	buf->data_u8[buf->write % buf->len] = byte;
	buf->write++;
}

static u8 ringbuffer_pop_byte(struct ringbuffer* buf)
{
	buf->read++;
	return buf->data_u8[(buf->read - 1) % buf->len];
}

int ringbuffer_push(struct ringbuffer* buf, void* inraw, size_t inlen)
{
	const char FUNCTION[]
		__attribute__ ((unused)) = "ringbuffer_push";		
	size_t a;
	size_t i;
	u8* in;
	if (!buf || !inraw) {
		return ENULLPTR;
	}
	if (!inlen) {
		return EEMPTY;
	}
	in = inraw;
	if (inlen > buf->len) {
		return EBUFOV;
	}
	/* Check if the buffer has enough total free space for the 
	   input */
	a = ringbuffer_available(buf);
	if (!buf->overflow && inlen > a) { 
		return EBUFOV;
	}
	for (i = 0; i < inlen; i++) {
		ringbuffer_push_byte(buf, in[i]);
	}
	return SUCCESS;
}

int ringbuffer_pop(struct ringbuffer* buf, void* outraw, size_t outlen)
{
	const char FUNCTION[]
		__attribute__ ((unused)) = "ringbuffer_push";	
	size_t r;
	size_t i;
	u8* out;
	if (!buf || !outraw) {
		return ENULLPTR;
	}
	out = outraw;
	r = ringbuffer_ready(buf);
	if (outlen < r) {
		/* Output does not have space for all available data */
		r = outlen;
	}
	for (i = 0; i < r; i++) {
		out[i] = ringbuffer_pop_byte(buf);
	}
	return r;
}
