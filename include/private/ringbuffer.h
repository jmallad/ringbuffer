#pragma once

struct ringbuffer {
	union {
		void* data;
		char* data_char;
		int* data_int;
		u8* data_u8;
	};
	size_t len;
	size_t read;
	size_t write;
	int overflow;
};

/* Run-time assertions - good idea to check this function before doing 
   anything */
int ringbuffer_assert();
/* Find the position of a character in a ringbuffer. This transforms unbounded
   read/write pointers to a bounded unsigned integer. */
size_t ringbuffer_findchar(struct ringbuffer* in, char c,
			   size_t i, size_t end);
int ringbuffer_findstring(size_t* out, struct ringbuffer* r, char* in,
			  size_t inlen);
/* Return how much total free space is currently in a ringbuffer */
size_t ringbuffer_available(struct ringbuffer* in);
/* Return how much data is in the ringbuffer to be read */
size_t ringbuffer_ready(struct ringbuffer* in);
int ringbuffer_new(struct ringbuffer** out, size_t len);
void ringbuffer_free(struct ringbuffer* in);
int ringbuffer_push(struct ringbuffer* buf, void* inraw, size_t inlen);
int ringbuffer_pop(struct ringbuffer* buf, void* outraw, size_t outlen);

